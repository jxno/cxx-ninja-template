# Ninja / C++ starter template

Project follows a very simple structure.

- **Libraries** - All application class _definitions_ / interface header files (third party et al.) go into `/include`.
- **Classes** - Application class _implementations_ go into `/lib`.
- **Source Code** - All application source goes to `/src`.
- **Documentation** - Application documentation is delegated to `/doc`.

The `build.ninja` file is extensible enough for you to link dependencies and source files as is.
Edit as needed.

Enjoy :hearts:
