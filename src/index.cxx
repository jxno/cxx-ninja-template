#include <iostream>

#ifndef PROJECT
#define PROJECT_NAME "Project Titan"
#define PROJECT_VERS "0.1.0"
#define PROJECT_AUTH "Kae"
#define PROJECT_LISC "ISC"
#endif

auto main(int argc, char* const argv[]) -> int {
  std::cout << "This is project :: " << PROJECT_NAME << "\n";
  std::cout << "Project version :: " << PROJECT_VERS << "\n";
  std::cout << "In file :: " << __FILE__ << "\n";
  std::cout << "This file was compiled on :: " << __DATE__ << "\n";
  std::cout.flush();
  return 0;
}
